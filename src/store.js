'use strict';

import { writable } from 'svelte/store';

export const dataStore = writable({});
export const didError = writable(false);
export const isLoading = writable(false);
