# Downballot Front End

A front end SPA for interacting with the [downballot service](https://gitlab.com/ProbablyRyan/downballot-backend). View the live deployment [here](https://downballot.ryanmchenry.com/).

## Screenshots

![Main address form](screenshots/Screenshot_Downballot_Main.png)
![Ballot display page](screenshots/Screenshot_Downballot_Display.png)

## Built With

* [Svelte](https://svelte.dev/docs)
* [Sass](https://sass-lang.com/documentation)
* [rollup.js](https://rollupjs.org/guide/en/)

## Quick Start Guide

1. Clone the repo
```sh
git clone git@gitlab.com:ProbablyRyan/downballot-spa.git
```
2. Install dependencies
```sh
npm install
```
3. Run the dev server
```sh
npm run dev
```